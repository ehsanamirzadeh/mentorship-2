# Mentorship Project

This is a project for improving data science skills contains 7 parts
* 1. Linux
* 2. Python
* 3. Data Analysis
* 4. NLP
* 5. Git
* 6. SQL
* 7. Spark

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need Jupiter notebook and python 3 to run python files
You need some libraries to run codes

```
import numpy as np
import pandas as pd
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
$ sudo install python
```


## Running the tests

You can launch codes in Jupiter notebook to see the results


## Built With

* Python 3
* Gitlab


## Contributing

Please email Ehsan.amirzadeh@gmail for details on our code of conduct, and the process for submitting pull requests to us.



## Authors

* Ehsan Amirzadeh




