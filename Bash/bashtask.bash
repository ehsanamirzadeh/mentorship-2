#part 1.1
cd /home/hezardastan/
mkdir -p Tasks/Bash/dir1
cd Tasks/Bash
touch 1.txt 2.py 3.pyc 4.pyc raw.txt dir1/test.pyc dir1/test2.pyc


#part 1.2
find . -name "*.pyc" -type f -delete


#part 1.3
mkdir desk
mv -v * desk 


#part 1.4
sudo apt install sl | sl


#part 1.5
#wget -P /home/hezardastan/Tasks/Bash/  wget http://wordpress.org/latest.tar.gz
tar -C /home/hezardastan/Tasks/Bash/desk/ -xvf /home/hezardastan/Desktop/bash-playground.tar.gz 


#part 1.6
#https://github.com/stedolan/jq/issues/805
cd /home/hezardastan/Tasks/Bash/desk/bash-playground/
jq -s '[.[][]]' * > TOTAL.json


#part 1.7
grep -vwE "(great|Great)" TOTAL.json > filtered.json


#part 1.8
wc -l foltered.json


#part1.9
du -k filtered.json


#part 1.10
grep "never" filtered.json | less


#part 1.11
tail -1000 filtered.json >> thousand.json


#part 1.12
df ./


#part 1.13
sudo apt install htop
htop ./

part 1.14
echo "green :used memory blue: Buffers  yellow/orang: cache"

