import click

@click.command()
@click.option('--n', default=3, help='Number of queries')

def Query(n):
        print("type EXIT to terminate the program :)")
        input_ = "start"

        dic_user = {}
        dic_task = {}

        while input_ != "EXIT" and n != 0:
            n = n - 1
            input_ = input()
            if input_ != "EXIT":
                if "CREAT USER " in input_:
                    user = input_.replace("CREAT USER ", "")
                    if user not in dic_user.keys():
                        dic_user[user] = []
                elif "CREAT TASK " in input_:
                    task = input_.replace("CREAT TASK ", "")
                    if task not in dic_task.keys():
                        dic_task[task] = []
                elif "ASSIGN " in input_:
                    input_ = input_.replace("ASSIGN ", "")
                    task = input_.split(" ")[0]
                    user = input_.split(" ")[1]
        #             print(task, user)
                    if (task not in dic_task.keys()) or (user not in dic_user.keys()):
                        print("USER OR TASK IS UNKNOWN!")
                    else:
                        dic_task[task].append(user)
                        dic_user[user].append(task)
                elif "LIST USER " in input_:
                    user = input_.replace("LIST USER ", "")
                    if (user not in dic_user.keys()):
                        print("USER IS UNKNOWN!")
                    else:
                        print("TASK LIST FOR THE USER IS: ",dic_user[user])

                elif "LIST TASK " in input_:
                    task = input_.replace("LIST TASK ", "")
                    if (task not in dic_task.keys()):
                        print("TASK IS UNKNOWN!")
                    else:
                        print("USER LIST FOR THE TASK IS: ",dic_task[task])

                else:
                    print("ERROR! THIS QUERY IS UNDEFINED",)

        # print(dic_user)
        # print(dic_task)





if __name__ == '__main__':
    Query()
